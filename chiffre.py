from tkinter.messagebox import *
from tkinter import * 

def get_entree_content():
        return entree.get("0.0",END).strip()

def get_valeur_decalage():
        return entree_decalage.get("0.0",END).strip()

def chiffrage():
        valeurdecalage = int(get_valeur_decalage)
        listeA = []
        liste = get_entree_content()
        label_mode = Label( text="Chiffree")
        label_mode.grid(column=2, row=2)
        
        for char in liste:
                char = ord(char)+valeurdecalage
                listeA.append(chr(char))
                sortie.delete(0.0,END)
                sortie.insert(0.0,listeA)



def dechiffrage():
        valeurdecalage = int(get_valeur_decalage)
        listeA = []
        liste = get_entree_content()
        label_mode = Label( text="Dechiffree")
        label_mode.grid(column=2, row=2)

        for char in liste:
                char = ord(char)-valeurdecalage
                listeA.append(chr(char))
                sortie.delete(0.0,END)
                sortie.insert(0.0,listeA)
                

root = Tk()

valeurdecalage=8

root.title("Cesar")

value = StringVar()
value.set('ABC')

label_titre = Label(root, text="Chiffre de Cesar")
label_titre.grid(column=2, row=0)

entree = Text(width='50',height='10')

entree.insert(0.0,value.get())
entree.grid(column=2, row=1)

entree_decalage = Text(width='5',height='1')
entree_decalage.grid(column=3, row=1)


button_chiffrage = Button(root, text="Chiffrage", command=chiffrage)
button_chiffrage.grid(row=2, column=1)

label_mode = Label(root, text="...")
label_mode.grid(column=2, row=2)

button_dechiffrage = Button(root, text="Dechiffrage", command=dechiffrage)
button_dechiffrage.grid(row=2, column=3)

sortie = Text(width='50',height='10')

sortie.grid(column=2, row=4)

root.mainloop()