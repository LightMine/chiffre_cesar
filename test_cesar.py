import unittest
import chiffre
class TestChiffreFunctions(unittest.TestCase):
    def test_chiffrage(self):
        self.assertEqual(chiffre.chiffrage("abc"), "ijk")
    def test_dechiffrage(self):
        self.assertEqual(chiffre.dechiffrage("ijk"), "abc")